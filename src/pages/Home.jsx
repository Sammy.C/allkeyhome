import React, {Component} from 'react'
import axios from 'axios'
import swal from 'sweetalert'

class Home extends Component{
      constructor(props) {
        super(props);
        this.submit = this.submit.bind(this)
      }
      state = {
        email: ''
      }
      componentDidUpdate(){
        if(this.state.valid && this.state.email && this.state.apiCall){
            swal({
                title: "Register",
                text: "You are official subscribed to our news letter",
                icon: "success",
                button: "Ok",
                closeOnClickOutside: false

            })
          .then((willDelete) => {
            if (willDelete) {
              this.setState({
                email: "",
                errorMessage: undefined,
                apiCall: undefined,
                valid:undefined
              })
            } 
          });
        }
        else{
            if(this.state.errorMessage && this.state.email && this.state.apiCall){
                swal({
                    title: "Warning",
                    text: "An error occured will subscrbing to our news letter. Please try again!",
                    icon: "warning",
                    button: "Ok",
                  })
                  .then((willDelete) => {
                    if (willDelete) {
                      this.setState({
                        email: "",
                        errorMessage: undefined,
                        apiCall: undefined,
                        valid:undefined
                      })
                    } 
                  });
            }
        }
    }
     submit(){
        if(this.state.email){
         
          axios.post('https://all-key.herokuapp.com/api/subscribe', {
            email: this.state.email 
          })
          .then(response => {
                this.setState({valid: response.data.valid, apiCall: true})
          })
          .catch(err =>  this.setState({errorMessage: true ,apiCall: true}))

        }
     } 


    render(){
        return(
            <main id="content" role="main">
            {/* Hero Section */}
            <div className="container space-2 space-top-md-5 space-bottom-md-3 space-top-lg-4">
              <div className="row justify-content-lg-between align-items-lg-center">
                <div className="col-lg-5 mb-4">
                  {/* Title */}
                  <div className="mb-3">
                    <h1 className="display-4 font-size-md-down-5 mb-0">Easy start,</h1>
                    <h2 className="text-primary display-3 font-size-md-down-5 font-weight-semi-bold">Easy setup</h2>
                  </div>
                  
                  {/* End Title */}
                </div>
                <div className="col-lg-6 mb-3">
                  {/* SVG Icon */}
                  <div id="SVGeasyStart" className=" d-none d-lg-block">
                    <figure className="ie-subscribe-2">
                      <img className="" src="../svg/flat-icons/enterprise-2.svg" alt="Description" data-parent="#SVGeasyStart" />
                    </figure>
                  </div>
                  {/* End SVG Icon */}
                </div>
              </div>
            </div>
            {/* End Hero Section */}
            <hr />
            {/* Mockup Blocks Section */}
        <div className="container space-2 space-md-3">
        {/* Title */}
        <div className="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
          <small className="text-secondary text-uppercase font-weight-medium mb-2">What we do?</small>
          <h2 className="h3 font-weight-medium">Simplify the life of a Landlord</h2>
        </div>
        {/* End Title */}
        <div className="row justify-content-lg-center">
          <div className="col-md-6 col-lg-5 mb-7">
            {/* Icon Blocks */}
            <div className="media pr-lg-5">
              <figure id="icon9" className=" ie-height-56 w-100 max-width-8 mr-4">
                <img className="" src="/svg/icons/icon-9.svg" alt="SVG" data-parent="#icon9" />
              </figure>
              <div className="media-body">
                <h3 className="h5">One touch away</h3>
                <p className="mb-1">We offer a simple to use application for both landlords and tenants, as well as for contractors, where each could conduct their daily activities </p>
                <a className="font-size-1" href="hm.html">Coming Soon <span className="fas fa-angle-right align-middle ml-2" /></a>
              </div>
            </div>
            {/* End Icon Blocks */}
          </div>
          <div className="col-md-6 col-lg-5 mb-7">
            {/* Icon Blocks */}
            <div className="media pl-lg-5">
              <figure id="icon3" className=" ie-height-56 w-100 max-width-8 mr-4">
                <img className="" src="/svg/icons/icon-3.svg" alt="SVG" data-parent="#icon3" />
              </figure>
              <div className="media-body">
                <h3 className="h5">Process</h3>
                <p className="mb-1">Through our automated processees we match the best tenants to your properties and make sure they are in the best shape to optimize rent revenue</p>
                <a className="font-size-1" href="hm.html">Coming Soon <span className="fas fa-angle-right align-middle ml-2" /></a>
              </div>
            </div>
            {/* End Icon Blocks */}
          </div>
          <div className="w-100" />
          <div className="col-md-6 col-lg-5 mb-7 mb-lg-0">
            {/* Icon Blocks */}
            <div className="media pr-lg-5">
              <figure id="icon5" className=" ie-height-56 w-100 max-width-8 mr-4">
                <img className="" src="/svg/icons/icon-5.svg" alt="SVG" data-parent="#icon5" />
              </figure>
              <div className="media-body">
                <h3 className="h5">Analytics</h3>
                <p className="mb-1">Get access to real time data on your properties with our interactive dashboard. Follow the trends and see how we optimize your rental income for you through our cutting-edge technology.</p>
                <a className="font-size-1" href="h.html">Coming Soon <span className="fas fa-angle-right align-middle ml-2" /></a>
              </div>
            </div>
            {/* End Icon Blocks */}
          </div>
          <div className="col-md-6 col-lg-5">
            {/* Icon Blocks */}
            <div className="media pl-lg-5">
              <figure id="icon2" className="ie-height-56 w-100 max-width-8 mr-4">
                <img className="" src="/svg/icons/icon-2.svg" alt="SVG" data-parent="#icon2" />
              </figure>
              <div className="media-body">
                <h3 className="h5">Reports</h3>
                <p className="mb-1">Get monthly and yearly reports on all your properties.
Tax reports will be generated through our up to date algorithms with tax regulation to optimize taxes
</p>
                <a className="font-size-1" href="htm.html">Coming Soon <span className="fas fa-angle-right align-middle ml-2" /></a>
              </div>
            </div>
            {/* End Icon Blocks */}
          </div>
        </div>
      </div>
      <div>
        {/* How It Works Section */}
        <div className="container space-2 space-md-3">
          {/* Title */}
          <div className="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
            <figure id="icon14" className=" ie-height-72 max-width-10 mx-auto mb-3">
              <img className="" src="/svg/icons/icon-14.svg" alt="SVG" data-parent="#icon14" />
            </figure>
            <h3 className="font-weight-medium">AllKey aims to make life easier for all parties involved in the rental market. May it be the landlord, the tenant or the contractor, renting will never have been this easy.</h3>
          </div>
          {/* End Title */}
          <div className="row align-items-lg-center">
            <div className="col-lg-6 order-lg-2 mb-7 mb-lg-0">
              <ul className="list-unstyled">
                {/* Info Block */}
                <li className="u-indicator-steps py-3">
                  <div className="media align-items-center border rounded p-5">
                    <div className="d-flex u-indicator-steps__inner mr-3">
                      <span className="display-4 text-primary font-weight-medium">1.</span>
                    </div>
                    <div className="media-body">
                      <p className="mb-0">Let us know a bit about your property online and set an appointment for appraisal as soon as 2 days later.</p>
                    </div>
                  </div>
                </li>
                {/* End Info Block */}
                {/* Info Block */}
                <li className="u-indicator-steps py-3">
                  <div className="media align-items-center border rounded p-5">
                    <div className="d-flex u-indicator-steps__inner mr-3">
                      <span className="display-4 text-primary font-weight-medium">2.</span>
                    </div>
                    <div className="media-body">
                      <p className="mb-0">A report will be sent to you in less than 48 hours following the visit of one of our agent
</p>
                    </div>
                  </div>
                </li>
                {/* End Info Block */}
                {/* Info Block */}
                <li className="u-indicator-steps py-3">
                  <div className="media align-items-center border rounded p-5">
                    <div className="d-flex u-indicator-steps__inner mr-3">
                      <span className="display-4 text-primary font-weight-medium">3.</span>
                    </div>
                    <div className="media-body">
                      <p className="mb-0">You're all set. Lay back, relax and watch your rental income get optimized</p>
                    </div>
                  </div>
                </li>
                {/* End Info Block */}
              </ul>
            </div>
            <div id="SVGhouseAgency" className="col-lg-6 order-lg-1">
              <div className="pr-lg-7">
                {/* SVG Icon */}
                <figure className="ie-house-agency">
                  <img className="" src="/svg/illustrations/house-agency.svg" alt=" Description" data-parent="#SVGhouseAgency" />
                </figure>
                {/* End SVG Icon */}
              </div>
            </div>
          </div>
        </div>
        {/* End How It Works Section */}
        {/* Divider */}
        <div className="container">
          <hr className="my-0" />
        </div>
        {/* End Divider */}
        {/* Icon Blocks Section */}
        <div className="container space-2 space-md-3">
          <div className="row justify-content-md-between">
            <div className="col-sm-6 offset-sm-3 col-lg-4 offset-lg-0 mb-9 mb-lg-0">
              {/* Icon Block */}
              <div className="text-center px-lg-5">
                <figure id="icon12" className="ie-height-72 max-width-10 mx-auto mb-3">
                  <img className="" src="/svg/icons/icon-12.svg" alt="SVG" data-parent="#icon12" />
                </figure>
                <h3 className="h5">Property guides</h3>
                <p>Stay up to date in the forever changing real estate industry through our monthly magazine</p>
                <div className="btn btn-sm btn-soft-primary transition-3d-hover" >
                  Coming Soon
                  <span className="fas fa-angle-right ml-2" />
                </div>
              </div>
              {/* End Icon Block */}
            </div>
            <div className="col-sm-6 col-lg-4 mb-9 mb-lg-0">
              {/* Icon Block */}
              <div className="text-center px-lg-5">
                <figure id="icon10" className="ie-height-72 max-width-10 mx-auto mb-3">
                  <img className="" src="/svg/icons/icon-10.svg" alt="SVG" data-parent="#icon10" />
                </figure>
                <h3 className="h5">Rewards Calculator</h3>
                <p>We want you to take informed decision when it comes to your business. Through this tool you will be able to have a clearer vison of where you are headed. <br /></p>
                <div className="btn btn-sm btn-soft-primary transition-3d-hover">
                  Coming Soon
                  <span className="fas fa-angle-right ml-2" />
                </div>
              </div>
              {/* End Icon Block */}
            </div>
            <div className="col-sm-6 col-lg-4">
              {/* Icon Block */}
              <div className="text-center px-lg-1">
                <figure id="icon11" className=" ie-height-72 max-width-10 mx-auto mb-3">
                  <img className="" src="/svg/icons/icon-11.svg" alt="SVG" data-parent="#icon11" />
                </figure>
                <h3 className="h5">Rent calculator</h3>
                <p>Through our data, we offer you forecast about your rental income so you will never have bad surprises and as for good ones they will help you better plan your next move and always stay ahead of the game.</p>
                <div className="btn btn-sm btn-soft-primary transition-3d-hover" href="">
                  Coming Soon
                  <span className="fas fa-angle-right ml-2" />
                </div>
              </div>
              {/* End Icon Block */}
            </div>
          </div>
        </div>
      <div className="space-bottom-2">
      <div className="container space-top-2 space-top-md-3">
        <div className="text-center w-md-60 mx-auto mb-7">
          <h2 className="text-primary"><span className="font-weight-semi-bold">Stay</span> in the know</h2>
          <p>Get special offers on the latest developments from All Key.</p>
        </div>

        <div className="w-md-50 w-lg-40 mx-auto">
          <div className="js-validate js-form-message">
            <label className="sr-only" >Enter your email address</label>
            <div className="input-group input-group-lg input-group-borderless input-group-pill shadow-sm">
              <input type="email" className="form-control" name="email" id="subscribeSrEmail" placeholder="Enter your email address" aria-label="Enter your email address" aria-describedby="subscribeButton" required  onChange={(e) => this.setState({ email: e.target.value.trim() })} value={this.state.email}  />
              <div className="input-group-append">
                <button className="btn btn-white text-primary" onClick={this.submit}>
                  <span className="fas fa-paper-plane"></span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

        </div>

        
      
          </main>
        )
    }
}
export default Home