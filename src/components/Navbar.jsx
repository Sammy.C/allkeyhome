import React,{Component} from 'react'


class Navbar extends Component {



    render(){
        return(
            <header id="header" className="u-header u-header--abs-top-md u-header--bg-transparent">
        <div className="u-header__section">
          <div id="logoAndNav" className="container">
            {/* Nav */}
            <nav className="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
              {/* Logo */}
              <a className="navbar-brand u-header__navbar-brand u-header__navbar-brand-center" href="index.html" aria-label="Front">
                <img className="js-svg-injector u-header__promo-icon" src="../img/logo.png" alt="SVG" />
                <span class="u-header__navbar-brand-text">AllKey</span>
              </a>
              {/* End Logo */}
              {/* Responsive Toggle Button */}
              <button type="button" className="navbar-toggler btn u-hamburger" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
                <span id="hamburgerTrigger" className="u-hamburger__box">
                  <span className="u-hamburger__inner" />
                </span>
              </button>
              {/* End Responsive Toggle Button */}
              {/* Navigation */}
              <div id="navBar" className="collapse navbar-collapse u-header__navbar-collapse">
                <ul className="navbar-nav u-header__navbar-nav">
                  {/* Home */}
                  <li className="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover" data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="left">
                    <p id="homeMegaMenu" className="nav-link u-header__nav-link u-header__nav-link-toggle" aria-haspopup="true" aria-expanded="false">Home</p>
                    {/* Home - Mega Menu */}
                    <div className="hs-mega-menu w-100 u-header__sub-menu" aria-labelledby="homeMegaMenu">
                      <div className="row no-gutters">
                        <div className="col-lg-6">
                          {/* Banner Image */}
                          <div className="u-header__banner" style={{backgroundImage: 'url(./img/750x750/img1.jpg)'}}>
                            <div className="u-header__banner-content">
                              <div className="mb-4">
                                <span className="u-header__banner-title">Branding Works</span>
                                <p className="u-header__banner-text">Experience a level of our quality in both design &amp; customization works.</p>
                              </div>
                              <div className="btn btn-primary btn-sm transition-3d-hover" >Learn More <span className="fas fa-angle-right ml-2" /></div>
                            </div>
                          </div>
                          {/* End Banner Image */}
                        </div>
                        <div className="col-lg-6">
                          <div className="row u-header__mega-menu-wrapper">
                            <div className="col-sm-6 mb-3 mb-sm-0">
                              <span className="u-header__sub-menu-title">Classic</span>
                              <ul className="u-header__sub-menu-nav-group mb-3">
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="index.html">Classic Agency</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="classic-business.html">Classic Business</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="classic-marketing.html">Classic Marketing</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="classic-consulting.html">Classic Consulting</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="classic-hosting.html">Classic Hosting</a></li>
                              </ul>
                              <span className="u-header__sub-menu-title">Corporate</span>
                              <ul className="u-header__sub-menu-nav-group">
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="corporate-agency.html">Corporate Agency</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="corporate-start-up.html">Corporate Start-Up</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="corporate-business.html">Corporate Business</a></li>
                              </ul>
                            </div>
                            <div className="col-sm-6">
                              <span className="u-header__sub-menu-title">Onepages</span>
                              <ul className="u-header__sub-menu-nav-group mb-3">
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="onepage-creative.html">Onepage Creative</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="onepage-saas.html">Onepage SaaS</a></li>
                              </ul>
                              <span className="u-header__sub-menu-title">Blog</span>
                              <ul className="u-header__sub-menu-nav-group mb-3">
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="blog-agency.html">Blog Agency</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="blog-start-up.html">Blog Start-Up</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="blog-business.html">Blog Business</a></li>
                              </ul>
                              <span className="u-header__sub-menu-title">Portfolio</span>
                              <ul className="u-header__sub-menu-nav-group">
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="portfolio-agency.html">Portfolio Agency</a></li>
                                <li><a className="nav-link u-header__sub-menu-nav-link" href="portfolio-profile.html">Portfolio Profile</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* End Home - Mega Menu */}
                  </li>
                  {/* End Home */}
                  {/* Pages */}
               
                  {/* End Pages */}
                  {/* Blog */}
                  
                  {/* End Blog */}
                  {/* Shop */}
                 
                  {/* End Docs */}
                  {/* Button */}
                  {/* End Button */}
                </ul>
              </div>
              {/* End Navigation */}
            </nav>
            {/* End Nav */}
          </div>
        </div>
      </header>
        )
    }
}
export default Navbar