import React, {Component} from 'react'


class Footer extends Component {
    render(){
        return(
            <footer className="bg-dark">
        <div className="container space-top-2">
          <div className="row justify-content-lg-between mb-7">
            <div className="col-6 col-md-4 col-lg-2 mb-7 mb-lg-0">
            
              {/* End List Group */}
            </div>
            <div className="col-6 col-md-4 col-lg-2 mb-7 mb-lg-0">
              {/* List Group */}
             
              {/* End List Group */}
            </div>
            <div className="col-6 col-md-4 col-lg-2 mb-7 mb-lg-0">
              <h4 className="h6 text-white">Contacts</h4>
              {/* Address */}
              <address className="list-group list-group-transparent list-group-white list-group-flush list-group-borderless mb-0">
                <p className="list-group-item list-group-item-action" >Montreal,Canada</p>
              </address>
              {/* End Address */}
            </div>
            <div className="col-sm-6 col-md-5 col-lg-3 col-lg-3">
              
            </div>
          </div>
          <div className="row justify-content-between align-items-center space-1">
            <div className="col-5">
              {/* Logo */}
              <div className="d-inline-flex align-items-center mb-3"  aria-label="Front">
               
                <span className="brand brand-light">AllKey</span>
              </div>
              {/* End Logo */}
            </div>
            <div className="col-6 text-right">
              <p className="small mb-0">© AllKey. 2020 </p>
            </div>
          </div>
        </div>
      </footer>
      )
    }
}
export default Footer